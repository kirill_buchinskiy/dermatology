function openModal(modalId, closeBtn) {
  var modal = document.getElementById(modalId);
  var closeBtn = document.getElementById(closeBtn);

  modal.style.display = "block";

  closeBtn.onclick = function() {
    modal.style.display = "none";
  };
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
}

var openCallOrderModal = document.getElementById("openCallOrderModal");
var getInsAndOutButton = document.getElementById("getInsAndOutButton");
var orderAppointmentButton = document.getElementById("orderAppointmentButton");
var orderAppointmentButton2 = document.getElementById(
  "orderAppointmentButton2"
);
var openDiaryOrderModal = document.getElementById("openDiaryOrderModal");
var footerFormSend = document.getElementById("footerFormSend");
var diaryOrderFormSend = document.getElementById("diaryOrderFormSend");
var callOrderFormSend = document.getElementById("callOrderFormSend");

openCallOrderModal.onclick = function() {
  openModal("callOrderModal", "closeCallOrderModal");
};
getInsAndOutButton.onclick = function() {
  openModal("callOrderModal", "closeCallOrderModal");
};
orderAppointmentButton.onclick = function() {
  openModal("callOrderModal", "closeCallOrderModal");
};
orderAppointmentButton2.onclick = function() {
  openModal("callOrderModal", "closeCallOrderModal");
};
openDiaryOrderModal.onclick = function() {
  openModal("diaryOrderModal", "closeDiaryOrderModal");
};
footerFormSend.onclick = function() {
  openModal("thanksModal", "closeThanksModal");
};
diaryOrderFormSend.onclick = function() {
  openModal("thanksModal", "closeThanksModal");
};
callOrderFormSend.onclick = function() {
  openModal("thanksModal", "closeThanksModal");
};
