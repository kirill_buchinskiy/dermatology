var certificate1 = document.getElementById("certificate1");
var certificate2 = document.getElementById("certificate2");
var certificate3 = document.getElementById("certificate3");
var certificate4 = document.getElementById("certificate4");

certificate1.onclick = function() {
  openCertificateModal(certificate1);
};
certificate2.onclick = function() {
  openCertificateModal(certificate2);
};
certificate3.onclick = function(certificate) {
  openCertificateModal(certificate3);
};
certificate4.onclick = function() {
  openCertificateModal(certificate4);
};

function openCertificateModal(certificate) {
  var certificateModalImg = document.getElementById("certificateModalImg");
  var closeCertificateModal = document.getElementById("closeCertificateModal");

  certificateModal.style.display = "block";
  certificateModalImg.src = certificate.getAttribute("src");
  closeCertificateModal.onclick = function() {
    certificateModal.style.display = "none";
  };
  window.onclick = function(event) {
    if (event.target == certificateModal) {
      certificateModal.style.display = "none";
    }
  };
}
